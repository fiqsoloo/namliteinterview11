import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import * as Highcharts from 'highcharts';

declare var require: any;
let Boost = require('highcharts/modules/boost');
let noData = require('highcharts/modules/no-data-to-display');
let More = require('highcharts/highcharts-more');

Boost(Highcharts);
noData(Highcharts);
More(Highcharts);
noData(Highcharts);

@Component({
  selector: 'app-graph-data',
  templateUrl: './graph-data.component.html'
})
export class GraphDataComponent {
  public forecasts: WeatherForecast[];

  public options: any = {
    data: {
      table: 'datatable'
    },
    chart: {
      type: 'column',
      styleMode: true,
    },
    title: {
      text: 'Weather Forecast'
    },
    credits: {
      enabled: false
    },
    tooltip: {
      formatter: function () {
        return 'x: ' + Highcharts.dateFormat('%e %b %y %H:%M:%S', this.x) +
          ' y: ' + this.y.toFixed(2);
      }
    },
    xAxis: {
      type: 'datetime',
      labels: {
        formatter: function () {
          return Highcharts.dateFormat('%e %b %y', this.value);
        }
      }
    },
    yAxis: {
      min: 0,
      title: {
        text: 'F/C '
      }
    },

    plotOptions: {
      column: {
        pointPadding: 0,
        borderWidth: 0,
        groupPadding: 0,
        shadow: false
      }
    },

    series: [
      {
        name: 'Celcius',
        data: [[new Date('2020-02-02 18:38:31').getTime(), 1], [new Date('2020-02-0 18:38:31').getTime(), 1], [new Date('2020-01-31 18:38:31').getTime(), 1], [new Date('2020-01-30 18:38:31').getTime(), 1], [new Date('2020-01-29 18:38:31').getTime(), 1],]
      }
        ,{
        name: 'Fahrenheit',
          data: [[new Date('2020-02-02 18:38:31').getTime(), 1], [new Date('2020-02-01 18:38:31').getTime(), 1], [new Date('2020-01-31 18:38:31').getTime(), 1], [new Date('2020-01-30 18:38:31').getTime(), 1], [new Date('2020-01-29 18:38:31').getTime(), 1],]
      }
      , 
    ]
  }
  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    http.get<WeatherForecast[]>(baseUrl + 'weatherforecast').subscribe(result => {
      this.forecasts = result;

      Highcharts.chart('chartcontainer', this.options);
    }, error => console.error(error));
  }
}

interface WeatherForecast {
  date: string;
  temperatureC: number;
  temperatureF: number;
  summary: string;
}

